var express = require('express')
var app = express()
var bodyParser = require('body-parser');
var http = require('http')
var socketio = require('socket.io');
var clients = []
var firebase = require("firebase");

// Initialize Firebase
// TODO: Replace with your project's customized code snippet
var config = {
    apiKey: "AIzaSyAw_eQqVdJS-atpKiwApi2u1PosVhrnlqU",
    authDomain: "edapt-aa8ec.firebaseapp.com",
    databaseURL: "https://edapt-aa8ec.firebaseio.com",
    projectId: "edapt-aa8ec",
    storageBucket: "edapt-aa8ec.appspot.com",
    messagingSenderId: "1097383442823"
};
firebase.initializeApp(config);

const port = 3000

var mongoose = require('mongoose');
mongoose.connect('mongodb://khuser:khpass02@localhost:27017/khdb');
// mongoose.connect('mongodb://localhost:27017/chatapp');


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
});

// get all data/stuff of the body (POST) parameters
// parse application/json 
app.use(bodyParser.json());

// parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

var server = http.Server(app);
var websocket = socketio(server);

websocket.on('connection', (socket) => {
    // console.log('A client just joined on', socket.id);
    require('./routes')(app, socket, clients, firebase)

    socket.on('logout', () => {
        socket.disconnect()
    })
});

// console.log('websocket',websocket)
server.listen(port, () => console.log('Started..'))

module.export = app

// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Headers", "*")
// });
// app.all('*', function (req, res, next) {
//     res.header('Access-Control-Allow-Origin', 'URLs to trust of allow');
//     res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//     res.header('Access-Control-Allow-Headers', 'Content-Type');
//     if ('OPTIONS' == req.method) {
//         res.sendStatus(200);
//     } else {
//         next();
//     }
// });
