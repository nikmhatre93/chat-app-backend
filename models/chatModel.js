var mongoose = require('mongoose')
var ObjectId = mongoose.Schema.Types.ObjectId

var chat = mongoose.Schema({
    participants: [{ type: ObjectId, ref: 'user' }],
    name: { type: String, default: "" },
    receiver_role: { type: String, default: "" },
    sender_role: { type: String, default: "" },
    count: { type: Number, default: 0 }
}, {
        toObject: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                return ret
            }
        },
        toJSON: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                return ret
            }
        }
    })


chat.virtual('messages', {
    ref: 'message',
    localField: '_id',
    foreignField: 'chat_id',
    justOne: false,
    options: { sort: { ts: -1 } }
})

// options: { sort: { ts: -1 }, limit: 10 }

chat.set('toObject', { virtuals: true });
chat.set('toJSON', { virtuals: true });

module.exports = mongoose.model("chat", chat);