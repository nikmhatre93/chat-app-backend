var mongoose = require('mongoose')
var ObjectId = mongoose.Schema.Types.ObjectId;
var mom = require('moment')

var block = mongoose.Schema({
    self_id: { type: ObjectId },
    user_id: { type: ObjectId },
    is_blocked: { type: Boolean, default: false }
},
    {
        toObject: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                return ret
            }
        },
        toJSON: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                return ret
            }
        }
    })



module.exports = mongoose.model("block", block);