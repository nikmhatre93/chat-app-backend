var mongoose = require('mongoose')
var ObjectId = mongoose.Schema.Types.ObjectId;
var mom = require('moment')

var message = mongoose.Schema({
    msg: { msg_type: String, msg: String },
    author: { type: ObjectId, ref: 'user_lists' },
    chat_id: { type: ObjectId },
    ts: { type: Date, default: Date.now() },
    is_deleted: [ObjectId],
    delivered: [ObjectId],
    read: { type: Boolean, default: false },
    sent: { type: Boolean, default: false }
},
    {
        toObject: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                return ret
            }
        },
        toJSON: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                return ret
            }
        }
    })



module.exports = mongoose.model("message", message);